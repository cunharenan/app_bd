package org.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

import org.model.Area;
import org.model.Bairro;
import org.model.Centro;
import org.model.Cidade;
import org.model.Colaborador;
import org.model.Curso;
import org.model.Emprestimo;
import org.model.Endereco;
import org.model.Fornecedor;
import org.model.Item;
import org.model.ItemDev;
import org.model.ItemEmp;
import org.model.ItemPedido;
import org.model.PedidoCompra;
import org.model.Uf;
import org.util.ModelController;

public class InsertImpl {

	ModelController _modelController;
	
	public InsertImpl()
	{
		_modelController = new ModelController();
	}
	
	public void insertAll() throws ParseException
	{
		System.out.println("Inserindo registros...");
	
		SimpleDateFormat _sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		//Inicio - Centros
		Centro _centro1 = new Centro("Centro T�cnologico", 1);
		_modelController.insert(_centro1);
		
		Centro _centro2 = new Centro("Centro de Sa�de", 1);
		_modelController.insert(_centro2);
		//Fim - Centros
		
		//Inicio - Cursos
		Curso _curso1 = new Curso(_centro1, "Engenharia de Computa��o", 1);
		_modelController.insert(_curso1);
		
		Curso _curso2 = new Curso(_centro2, "Medicina", 1);
		_modelController.insert(_curso2);
		//Fim - Cursos
		
		//Inicio - Ufs
		Uf _uf1 = new Uf("SC", "Santa Catarina", 1);
		_modelController.insert(_uf1);
		
		Uf _uf2 = new Uf("RS", "Rio Grande do Sul", 1);
		_modelController.insert(_uf2);
		//Fim - Ufs
		
		//Inicio - Cidades
		Cidade _cidade1 = new Cidade(_uf1, "Crici�ma", 1);
		_modelController.insert(_cidade1);
		
		Cidade _cidade2 = new Cidade(_uf2, "Porto Alegre", 1);
		_modelController.insert(_cidade2);
		//Fim - Cidades
		
		//Inicio - Bairros
		Bairro _bairro1 = new Bairro(_cidade1, "Centro", 1);
		_modelController.insert(_bairro1);
		
		Bairro _bairro2 = new Bairro(_cidade2, "Tche", 1);
		_modelController.insert(_bairro2);
		//Fim - Bairros
		
		//Inicio - Enderecos
		Endereco _endereco1 = new Endereco(_bairro1, "88811812", "Rua Vit�rio Serafim, 308", 1);
		_modelController.insert(_endereco1);
		
		Endereco _endereco2 = new Endereco(_bairro2, "12311678", "Avenida Amarildo Joao, 57", 1);
		_modelController.insert(_endereco2);
		
		Endereco _endereco3 = new Endereco(_bairro2, "12311678", "Avenida Centen�rio, 143", 1);
		_modelController.insert(_endereco3);
		//Fim - Enderecos
		
		//Inicio - Colaboradores
		Colaborador _colaborador1 = new Colaborador(_centro1,
													_endereco1, 
													"Renan Cunha", 
													"100100100", 
													"0889345676-1", 
													"5671899", 
													"M", 
													_sdf.parse("10/02/1994"), 
													new Date(), 
													"renan.bara@hotmail.com", 
													1);
		_modelController.insert(_colaborador1);
		
		Colaborador _colaborador2 = new Colaborador(_centro2,
				_endereco2, 
				"Rodson Vants", 
				"100100100", 
				"0889345676-1", 
				"5671899", 
				"M", 
				_sdf.parse("10/02/1990"), 
				new Date(), 
				"rodvants@hotmail.com", 
				1);
		_modelController.insert(_colaborador2);
		//Fim - Colaboradores
		
		//Inicio - Areas
		Area _area1 = new Area("Geral", 1);
		_modelController.insert(_area1);
		
		Area _area2 = new Area("Mec�nica", 1);
		_modelController.insert(_area2);	
		
		Area _area3 = new Area("Eletr�nica", 1);
		_modelController.insert(_area3);
		//Fim - Areas
		
		//Inicio - Itens
		Item _item1 = new Item("Resistor 10kohm", "1010101010", "10101010", 100, 5, 10);
		HashSet<Area> _item1Areas = new HashSet<Area>();
		_item1Areas.add(_area3);
		_item1Areas.add(_area1);
		_item1.setAreas(_item1Areas);
		_item1.setReutilizavel(1);
		_modelController.insert(_item1);
		
		Item _item2 = new Item("Retroprojetor HP JAWUH", "12345677", "12345677", 15, 5, 2);
		HashSet<Area> _item2Areas = new HashSet<Area>();
		_item2Areas.add(_area1);
		_item2.setAreas(_item2Areas);
		_item2.setReutilizavel(1);
		_modelController.insert(_item2);
		
		Item _item3 = new Item("Multimetro Megaboss", "1234512345", "1234512345", 20, 1, 20);
		HashSet<Area> _item3Areas = new HashSet<Area>();
		_item3Areas.add(_area3);
		_item3Areas.add(_area1);
		_item3.setAreas(_item3Areas);
		_item3.setReutilizavel(1);
		_modelController.insert(_item3);
		
		Item _item4 = new Item("Chave de Fenda AB", "1234512345", "1234512345", 3, 1, 3);
		HashSet<Area> _item4Areas = new HashSet<Area>();
		_item4Areas.add(_area2);
		_item4Areas.add(_area1);
		_item4.setAreas(_item4Areas);
		_item4.setReutilizavel(1);
		_modelController.insert(_item4);
		//Fim - Itens
		
		//Inicio - Fornecedores
		Fornecedor _forn1 = new Fornecedor(_endereco2, "Iluminati Forn LTDA", "Iluminatis Ferramentas", "1012012012", 1);
		_modelController.insert(_forn1);
		
		Fornecedor _forn2 = new Fornecedor(_endereco3, "Mascada Equipamentos LTDA", "Mascada Equipamentos", "147258369", 1);
		_modelController.insert(_forn2);
		//Fim - Fornecedores
		
		//Inicio - Emprestimo
		Emprestimo _emp1 = new Emprestimo(_colaborador1, _sdf.parse("06/11/2015"), 1);
		_emp1.getItemEmps().add(new ItemEmp(_emp1, _item1, BigDecimal.valueOf(5.0), 1));
		_modelController.insert(_emp1);
		
		Emprestimo _emp2 = new Emprestimo(_colaborador2, _sdf.parse("01/11/2015"), 1);
		_emp2.getItemEmps().add(new ItemEmp(_emp2, _item1, BigDecimal.valueOf(5.0), 1));
		_modelController.insert(_emp2);
		
		Emprestimo _emp3 = new Emprestimo(_colaborador2, _sdf.parse("15/10/2015"), 1);
		_emp3.getItemEmps().add(new ItemEmp(_emp3, _item3, BigDecimal.valueOf(1), 1));
		_emp3.getItemEmps().add(new ItemEmp(_emp3, _item4, BigDecimal.valueOf(1), 1));
		_modelController.insert(_emp3);
		
		Emprestimo _emp4 = new Emprestimo(_colaborador1, _sdf.parse("17/07/2015"), 1);
		_emp4.getItemEmps().add(new ItemEmp(_emp4, _item3, BigDecimal.valueOf(1), 1));
		_emp4.getItemEmps().add(new ItemEmp(_emp4, _item4, BigDecimal.valueOf(2), 1));
		_modelController.insert(_emp4);
		
		Emprestimo _emp5 = new Emprestimo(_colaborador1, _sdf.parse("12/05/2015"), 1);
		_emp5.getItemEmps().add(new ItemEmp(_emp5, _item3, BigDecimal.valueOf(18), 1));
		_emp5.getItemEmps().add(new ItemEmp(_emp5, _item2, BigDecimal.valueOf(2), 1));
		_modelController.insert(_emp5);
		//Fim - Emprestimo
		
		//Inicio - Devolu��o de empr�stimos
		for(Iterator<ItemEmp> it = _emp3.getItemEmps().iterator(); it.hasNext(); )
		{
			ItemEmp _itemEmp = it.next();
			_itemEmp.getItemDevs().add(new ItemDev(_itemEmp, _itemEmp.getQtde(), _sdf.parse("18/10/2015"), 1));
		}
		_modelController.update(_emp3);
		//Fim - Devolu��o de empr�stimos
		
		//Inicio - Pedido de compra
		PedidoCompra _ped1 = new PedidoCompra(_colaborador1, _forn1, _sdf.parse("25/02/2015"), 1);
		_ped1.getItemPedidos().add(new ItemPedido(_item1, _ped1, BigDecimal.valueOf(20.0), 1));
		_ped1.getItemPedidos().add(new ItemPedido(_item3, _ped1, BigDecimal.valueOf(1), 1));
		_modelController.insert(_ped1);
		
		PedidoCompra _ped2 = new PedidoCompra(_colaborador2, _forn2, _sdf.parse("09/04/2015"), 1);
		_ped2.getItemPedidos().add(new ItemPedido(_item1, _ped2, BigDecimal.valueOf(50.0), 1));
		_ped2.getItemPedidos().add(new ItemPedido(_item2, _ped2, BigDecimal.valueOf(2.0), 1));
		_ped2.getItemPedidos().add(new ItemPedido(_item3, _ped2, BigDecimal.valueOf(1.0), 1));
		_modelController.insert(_ped2);
		
		
		PedidoCompra _ped3 = new PedidoCompra(_colaborador1, _forn1, _sdf.parse("19/07/2015"), 1);
		_ped3.getItemPedidos().add(new ItemPedido(_item4, _ped3, BigDecimal.valueOf(1.0), 1));
		_modelController.insert(_ped3);
		//Fim - Pedido de compra
		
		System.out.println("Todos registros foram inseridos...");
	}
	

	
}
