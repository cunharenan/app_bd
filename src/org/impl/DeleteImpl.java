package org.impl;

import org.hibernate.Session;
import org.util.GeneralController;
import org.util.ModelController;

public class DeleteImpl {

	private Session _session;
	
	public DeleteImpl()
	{
		this._session = GeneralController.getSession();
	}
	
	public void deleteAll()
	{
		System.out.println("Limpando todas tabelas...");
		this._session.beginTransaction();
		this._session.createSQLQuery("delete from item_dev").executeUpdate();
		this._session.createSQLQuery("delete from item_emp").executeUpdate();
		this._session.createSQLQuery("delete from item_pedido").executeUpdate();
		this._session.createSQLQuery("delete from material_area").executeUpdate();
		this._session.createSQLQuery("delete from item").executeUpdate();
		this._session.createSQLQuery("delete from area").executeUpdate();
		this._session.createSQLQuery("delete from pedido_compra").executeUpdate();
		this._session.createSQLQuery("delete from fornecedor").executeUpdate();
		this._session.createSQLQuery("delete from emprestimo").executeUpdate();
		this._session.createSQLQuery("delete from curso").executeUpdate();
		this._session.createSQLQuery("delete from colaborador").executeUpdate();
		this._session.createSQLQuery("delete from centro").executeUpdate();
		this._session.createSQLQuery("delete from endereco").executeUpdate();
		this._session.createSQLQuery("delete from bairro").executeUpdate();
		this._session.createSQLQuery("delete from cidade").executeUpdate();
		this._session.createSQLQuery("delete from uf").executeUpdate();
		this._session.getTransaction().commit();
		System.out.println("Todas tabelas foram limpas...");
		
		this._resetSequence();
	}
	
	private void _resetSequence()
	{
		System.out.println("Resetando os sequences de todas as tabelas...");
		this._session.beginTransaction();
		this._session.createSQLQuery("ALTER TABLE item_dev AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE item_emp AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE item_pedido AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE material_area AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE item AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE area AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE pedido_compra AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE fornecedor AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE emprestimo AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE curso AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE centro AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE colaborador AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE endereco AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE bairro AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE cidade AUTO_INCREMENT = 1").executeUpdate();
		this._session.createSQLQuery("ALTER TABLE uf AUTO_INCREMENT = 1").executeUpdate();
		this._session.getTransaction().commit();
		System.out.println("Os sequences de todas as tabelas foram resetados...");
	}
	
}
