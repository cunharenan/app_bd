package org.controller;

import java.text.ParseException;

import org.impl.DeleteImpl;
import org.impl.InsertImpl;

public class Principal {
	
	public static void main(String[] args) throws ParseException {
	
		//Limpar todas tabelas
		DeleteImpl _deleteImpl = new DeleteImpl();
		_deleteImpl.deleteAll();
		
		//Inserindo registros nas tabelas
		InsertImpl _insertImpl = new InsertImpl();
		_insertImpl.insertAll();
		
	}

}
